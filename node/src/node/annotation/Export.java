package node.annotation;

/**
 * Specified the name to export
 * @author Deepak Patil
 *
 */
public @interface Export {
	String name();
}
