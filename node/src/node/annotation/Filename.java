package node.annotation;


public @interface Filename {
	String name();
}
