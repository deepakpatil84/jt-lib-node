package node.core;

import com.jstype.fx.NativeNonInstanciable;
import com.jstype.fx.NoJavaScript;


public class Console {

	private Console(){
		
	}
	
	public static native Console getConsole()
	/*-{
	 return console;
	}-*/;

	@NoJavaScript
	public native static void log(String message)
	/*-{
	  console.log(message);
	}-*/;
	
	@NoJavaScript
	public native static void log(long num);

	
	@NoJavaScript
	public native static void log(Object obj)
	/*-{
	  console.log(obj);
	}-*/;

}
