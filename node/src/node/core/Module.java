package node.core;

import com.jstype.fx.NativeNonInstanciable;

/**
 * (function (exports, require, module, __filename, __dirname) { 
 *
 */
public abstract class Module {
	protected Module(){
		
	}
	protected native Module getModuleObj()
	/*-{
	 	return this.module;
	 }-*/;
	
}
