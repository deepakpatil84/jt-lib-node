package node.core;

import com.jstype.core.JSObject;
//@NativeNonInstanciable(proto = "(process.prototype=process)")
public class Process {
	private Process() {
	}

	public static native Process getPrcess()
	/*-{
	 	return process;
	 }-*/;

	/**
	 * An array containing the command line arguments. The first element will be 'node', the second element will be the name of the JavaScript file. The next elements will be any
	 * additional command line arguments.
	 * 
	 * @return
	 */

	public static native String[] getArgs()
	/*-{
		return process.argv;
	}-*/;

	/**
	 * This is the absolute pathname of the executable that started the process.
	 * 
	 * @return
	 */
	public static native String getExecPath()
	/*-{
		return process.execPath;
	}-*/;

	/**
	 * This is the set of node-specific command line options from the executable that started the process. These options do not show up in process.argv, and do not include the node
	 * executable, the name of the script, or any options following the script name. These options are useful in order to spawn child processes with the same execution environment
	 * as the parent.
	 * 
	 * @return
	 */
	public static native String[] geArguments()
	/*-{
		return process.argv;
	}-*/;
	
	public static native String getArch()
	/*-{
	 	return process.arch;
	 }-*/;
	
	public static native JSObject getEnv()
	/*-{
	 	return process.env;
	}-*/;
	
	public static native JSObject getFeatures()
	/*-{
	 	return process.features;
	}-*/;

	/**
	 * This causes node to emit an abort. This will cause node to exit and generate a core file.
	 */
	public static native void abort()
	/*-{
	 	process.abort();
	}-*/;

	public static native void exit()
	/*-{
	  	process.exit();
	}-*/;

	public static native void exit(int code)
	/*-{
	 	process.exit(code);
	}-*/;
	
	public static native JSObject getVersions()
	/*-{
	 	return process.versions;
	}-*/;
	
	public static native int getPid()
	/*-{
	 return process.pid;
	}-*/;
	
	//TODO:native method throws an exception here, wrap that in corresponding java Exception
	public static native void chdir(String path)
	/*-{
	 return process.chdir(path)
	}-*/;
	

	public static native String cwd()
	/*-{
	 	return process.cwd();
	}-*/;
	
	public static native int uptime()
	/*-{
	 return process.uptime;
	}-*/;
	
	
	
	
	

}
