package node.modules;

import node.core.Module;

public class FS  extends Module{
	public static interface ErrorHandler{
		void onError(String error);
	}
	
	public static class FileDescriptor{
		
	}
	
	public native static String renameSync(String oldPath,String newPath)
	/*-{
	 	return fs.renameSync(oldPath,newPath);
	}-*/;
	
	public native static void rename(String oldPath,String newPath, ErrorHandler handler)
	/*-{
	 	fs.rename(oldPath,newPath,function(err){
	 		handler.@node.modules.FS.ErrorHandler::onError(Ljava/lang/String)(err);
	 	});
	}-*/;
	
	public native static void ftruncate(FileDescriptor fd,int len,ErrorHandler handler)
	/*-{
	 	fs.ftruncate(fd,len,function(err){
	 		handler.@node.modules.FS.ErrorHandler::onError(Ljava/lang/String)(err);
	 	});
	}-*/;
	
	public native static String ftruncateSync(FileDescriptor fd,int len)
	/*-{
	 	return fs.ftruncateSync(fd,len);
	}-*/;
	
	public native static void truncate(String path,int len,ErrorHandler handler)
	/*-{
	 	fs.truncate(path,len,function(err){
	 		handler.@node.modules.FS.ErrorHandler::onError(Ljava/lang/String)(err);
	 	});
	}-*/;
	
	public native static String truncateSync(String path,int len)
	/*-{
	 	return fs.truncateSync(path,len);
	}-*/;
	
	
	
	public native static void chown(String path,String uid,String gid,ErrorHandler handler)
	/*-{
	 	fs.chown(path,uid,gid,function(err){
	 		handler.@node.modules.FS.ErrorHandler::onError(Ljava/lang/String)(err);
	 	});
	}-*/;
	
	public native static String chownSync(String path,String uid,String gid)
	/*-{
	 	return fs.chownSync(path,uid,gid);
	}-*/;
	
	public native static void fchown(FileDescriptor fd,String uid,String gid,ErrorHandler handler)
	/*-{
	 	fs.fchown(fd,uid,gid,function(err){
	 		handler.@node.modules.FS.ErrorHandler::onError(Ljava/lang/String)(err);
	 	});
	}-*/;
	
	public native static String fchownSync(FileDescriptor fd,String uid,String gid)
	/*-{
	 	return fs.fchownSync(fd,uid,gid);
	}-*/;
	
	public native static void lchown(String path,String uid,String gid,ErrorHandler handler)
	/*-{
	 	fs.lchown(path,uid,gid,function(err){
	 		handler.@node.modules.FS.ErrorHandler::onError(Ljava/lang/String)(err);
	 	});
	}-*/;
	
	public native static String lchownSync(String path,String uid,String gid)
	/*-{
	 	return fs.lchownSync(path,uid,gid);
	}-*/;
	
	public native static void chmod(String path,String mode,ErrorHandler handler)
	/*-{
	 	fs.chmod(path,mode,function(err){
	 		handler.@node.modules.FS.ErrorHandler::onError(Ljava/lang/String)(err);
	 	});
	}-*/;
	
	public native static String chmodSync(String path,String mode)
	/*-{
	 	return fs.chmodSync(path,mode);
	}-*/;
	
	public native static void fchmod(FileDescriptor fd,String mode,ErrorHandler handler)
	/*-{
	 	fs.fchmod(fd,mode,function(err){
	 		handler.@node.modules.FS.ErrorHandler::onError(Ljava/lang/String)(err);
	 	});
	}-*/;
	
	public native static String fchmodSync(FileDescriptor fd,String mode)
	/*-{
	 	return fs.fchmodSync(fd,mode);
	}-*/;
	
}
